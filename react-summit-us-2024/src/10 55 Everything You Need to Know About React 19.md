# 10:55 - [Summit Track] Everything You Need to Know About React 19

Shruti KAPOOR

## Abstract

> React 19 introduces Actions, a new way to simplify form management by simplifying form handling, adding loading
> states, and making data retrieval easier. This hands-on talk explores transitioning to React 19 using Form Actions,
> with code demos of managing form submission, displaying loading indicators, and retrieving form data efficiently.

## Notes

### React Actions

- Simplifient la gestion des opérations asynchrones avec le hook `startTransition`, qui marque certaines
  mises à jour comme non urgentes pour améliorer les performances.
- "Functions that use async transitions are actions"

```typescript jsx

function UpdateName({}) {
    const [name, setName] = useState("");
    const [error, setError] = useState(null);
    const [isPending, startTransition] = useTransition();

    const handleSubbmit = () => {
        startTransition(async () => {
            // Action:
            const error = await updateName(name);
            if (error) {
                setError(error);
                return;
            }
            redirect("/path")
        });
    }

    return (
        <div>
            <input value={name} onChange={(event) => setName(event.target.value)}/>
            <button onClick={handleSubbmit} disabled={isPending}>
                Update
            </button>
            {error && <p>{error}</>}
        </div>
    );
}
```

- On peut aussi attacher directement une action à un `form`

```typescript jsx
<form action={actionFunction}/>
```

### React Compiler

- Plus besoin d'utiliser `useMemo` ni `useCallback`, le compilateur fait de l'auto-memoization
    - Pas nécessaire de les enlever si on en a dans le code
    - La mémoization auto est différente de `useMemo` et `useCallback`
- Optimisation des re-renders:
    - Skip le re-render des enfants quand seul le parent change
    - Skip les calculs coûteux

### `useFormStatus`

```typescript jsx
const {pending, data, method, action} = useFormStatus();
```

- S'utiliser uniquement dans l'enfant d'un `form`

```typescript jsx
// KO
function Form() {
    const {pending} = useFormStatus();
    return <Form action={submit}/>;
}

// OK
function Form() {
    return (
        <form action={submit}>
            <Submit/>
        </form>
    );
}

function Button() {
    const {pending} = useFormStatus();
    return <button disabled={pending}/>;
}
```

### `useActionState`

- Donne un statut mis à jour en fonction d'une action

```typescript jsx
const [state, formAction] = useActionState(fn, initialState, permaLink ?);
```

```typescript jsx
function MyComponent() {
    const [state, formAction] = useActionState(action, null);

    return (
        <form action={formAction}>
            ...
        </form>
    );
}
```

### `useOptimistic`

```typescript jsx
const [optimisticState, addOptimistic] = useOptimistic(state, updateFn);
```

- Permet de faire une mise à jour de façon _optimiste_
    - Par exemple, si on clique sur un bouton "Like", on peut être
      optimiste et afficher le bouton cliqué en attendant la réponse du serveur - et quand on reçoit sa répondre, mettre
      à jour en fonction de ce qu'il s'est réellement passé

### API `use`

- Ressemble à un hook mais n'en est pas un

```typescript jsx
const theme = use(ThemeContext);
const comments = use(commentsPromise);
```