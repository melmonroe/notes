# 10:15 - [Base Camp Track] Maintaining a Library and a Community

Mark ERIKSON

## Abstract

> Anyone can publish a library to NPM. But what happens when that library is used by millions of developers? How do you
> juggle the complexities of publishing one of the most widely used packages in the ecosystem, and also deal with
> supporting and maintaining a community?
>
> We'll look at the mindset and approach to dealing with these challenges and what it means to be a "maintainer"
> today, including practices for providing user support across platforms, keeping a "devrel" mindset, designing
> documentation, designing features and APIs, how to consider package versioning and compatibility, when to ship
> breaking
> changes, technical challenges with publishing packages, and keeping up with the ever-evolving ecosystem.

## Notes

Dichotomie de l'open-source entre :

- le contrat légal : j'écris du code, il est accessible gratuitement
- les attentes implicites : celui a écrit le code fait aussi le support

> Maintaining an open-source project means being a developer, manager, designer, dev advocate, tech support, sales
> person, community manager, content creator, technical writer, and humain being, all at the same time.
>
> -- Artem ZAKHARCHENKO

- Il est important de montrer aux gens qu'on les écoute en faisant du **support**.
    - Il faut être disponible tout en fixant des limites claires et en évitant le burnout
- La **documentation** est souvent plus importante que le code.
    - Quatre catégories :
        - Tutoriels
        - _How-to_
        - Explications
        - Références
    - Il n'y a jamais trop de documentation
    - Garder la cible en tête quand on écrit la doc
- Il faut **_designer_** les features en ayant en tête les besoins de l'utilisateur
- Les contributions externes sont une resource inestimable, mais demandent beaucoup d'attention lors des relectures.
    - Ecrire un guide de contribution peut aider à simplifier le process
- Produire des **release notes** et des changelogs détaillés pour guider les utilisateurs lors des mises à jour
- Les **_breaking changes_** sont parfois inévitables, mais on peut diminuer la gêne occasionée en écrivant par
  exemples de scripts de mise à jour automatique