# 15:35 - [Summit Track] What Refs Can Do for You

Stephen COOPER

## Abstract

> While Refs are considered an escape hatch there are times when you might just need to use them.In this talk, with the
> help of real world examples, we will look at what Refs can do for us in terms of code clarity and also performance
> gains. Think along the lines of minimising renders and avoiding flickering UIs. We will cover the restrictions for
> safe
> usage and for those of you already using these patterns make sure you are prepared for the changes coming in React 19.

## Notes
Redite de la conférence "Patterns for Performance" donnée à [React Advanced](../../react-advanced-2023/README.md) en 2023.