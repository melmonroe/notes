# React Summit US 2024

Le mardi 19 novembre 2024 au Liberty Science Center, New York et le vendredi 22 novembre 2024 en distanciel.

## Mardi

(09:15 - [Summit Track] Opening Ceremony)

[10:15 - [Base Camp Track] Maintaining a Library and a Community](src/10%2015%20Maintaining%20a%20Library%20and%20a%20Community.md)

[10:55 - [Summit Track] Everything You Need to Know About React 19](src/10%2055%20Everything%20You%20Need%20to%20Know%20About%20React%2019.md)

[~~15:35 - [Summit Track] What Refs Can Do for You~~](src/15%2035%20What%20Refs%20Can%20Do%20for%20You.md)

(17:25 - [Summit Track] Closing Ceremony)
