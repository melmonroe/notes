# React Summit 2023

Le vendredi 2 juin 2023 à the Kromhouthal, Amsterdam et le mardi 6 juin 2023 en distanciel.

## TL;DR

* Retour prochain des **@decorators** dans React
* **Tailwind CSS** très populaire
* **shadcn/ui**, nouveau framework d'UI (basé sur Tailwind)
* **Replay.io**, application de débug (cloud only)
* **Codux**, nouvel IDE dédié à React (cloud only, israëlien)
* Deux nouveaux frameworks JavaScript, **Qwik** et **fresh**, ont l'air assez populaires
* Salon très orienté networking ; le salon **React Advanced** organisé à Londres fin octobre semble plus intéressant d'un point de vue technique

![pres1.jpg](img/pres1.jpg){width=200px}
![pres2.jpg](img/pres2.jpg){width=200px}
![pres4.jpg](img/pres4.jpg){width=200px}
![pres8.jpg](img/pres8.jpg){width=200px}

![pres3.jpg](img/pres3.jpg){width=200px}
![pres5.jpg](img/pres5.jpg){width=200px}
![pres6.jpg](img/pres6.jpg){width=200px}
![pres7.jpg](img/pres7.jpg){width=200px}

## Vendredi

(08:00 - Registration)

09:10 - [Summit Track] Opening Ceremony

[09:20 - [Summit Track] Reactivity There and Back Again (Keynote)](src/09%2020%20-%20%5BSummit%20Track%5D%20Reactivity%20There%20and%20Back%20Again.md)

[09:40 - Anniversaire de React](src/09%2040%20-%20Anniversaire%20de%20React.md)

[09:50 - [Summit Track] Improving Developer Happiness with AI](src/09%2050%20-%20%5BSummit%20Track%5D%20Improving%20Developer%20Happiness%20with%20AI.md)

[10:20 - [Summit Track] Debugging JS](src/10%2020%20-%20%5BSummit%20Track%5D%20Debugging%20JS.md)

(10:50 - Coffee Break)

[11:20 - [Summit Track] Principles for Scaling Frontend Application Development](src/11%2020%20-%20%5BSummit%20Track%5D%20Principles%20for%20Scaling%20Frontend%20Application%20Development.md)

[~~11:50 - [Base Camp Track] React Concurrency Explained~~](src/11%2050%20-%20%5BBase%20Camp%20Track%5D%20React%20Concurrency%20Explained.md)

[12:20 - [Summit Track] You Can't Use Hooks Conditionally... or Can You?](src/12%2020%20-%20%5BSummit%20Track%5D%20You%20Can%27t%20Use%20Hooks%20Conditionally.md)

~~(12:50 - Lunch)~~

[13:40 - [Base Camp Track] Should You Use React in 2023](src/13%2040%20-%20%5BBase%20Camp%20Track%5D%20Should%20You%20Use%20React%20in%202023.md)

[14:20 - [Summit Track] Modern Approaches for Creating](src/14%2020%20-%20%5BSummit%20Track%5D%20Modern%20Approaches%20for%20Creating.md)

[~~14:50 - [Base Camp Track] Supercharging React Apps with WASM~~](src/14%2050%20-%20%5BBase%20Camp%20Track%5D%20Supercharging%20React%20Apps%20with%20WASM.md)

(15:20 - Coffee Break)

[15:50 - [Summit Track] Off with Their Heads](src/15%2050%20-%20%5BSummit%20Track%5D%20Off%20with%20Their%20Heads.md)

[16:20 - [Summit Track] Open Source Awards Ceremony](src/16%2020%20-%20%5BSummit%20Track%5D%20Open%20Source%20Awards%20Ceremony.md)

[16:50 - [Summit Track] How Not to Build a Video Game](src/16%2050%20-%20%5BSummit%20Track%5D%20How%20Not%20to%20Build%20a%20Video%20Game.md)

17:20 - [Summit Track] Closing Ceremony

(17:30 - Break before the party)

(19:00 - Afterparty)
