# 13:40 - [Base Camp Track] Should You Use React in 2023?

Tru NARLA (Discord, USA) <[@trunarla](https://twitter.com/trunarla)> - Jordan GENSLER (Mysten
Labs, USA) <[@vapejuicejordan](https://twitter.com/vapejuicejordan)>

## Abstract

> Meta frameworks are increasingly popular.
People dunk on React all the time.
Are you insane for still using React?
The talk is going to cover how real companies make this evaluation of which framework to choose.
It's talking about the advantages of using React, primarily focusing on the positives but also offering constructive
thoughts on why you might not want to.Should you use React in 2023?

## Notes

Peut-être.

Quelques bonnes raisons d'utiliser React :

* Familiarité (On connait déjà donc c'est plus simple, c'est facile de recruter...)
* Ressources (Communauté...)
* Écosystème (NextJS, Remix, Storybook, Redux...)
* Performances (Contrairement à ce qu'on entend souvent, React a plutôt de bonnes perfs en général.
  Après il peut avoir ses problèmes, mais comme tous les outils)
* React Native (On peut partager du code avec une version mobile de notre appli ; cf.
  Discord)

En fin de compte, ça dépend de ce que qui est important pour nous.

Il faut éviter de se laisser distraire par toutes les nouvelles choses qui brillent et éviter le "FOMO driven
development".