# 14:20 - [Summit Track] Modern Approaches for Creating Extremely Fast Websites

Brad WESTALL (ReactTraining, USA) <[@bradwestfall](https://twitter.com/bradwestfall)>

## Abstract

> In this talk is focused on performance-optimizations and standards-based approaches to achieving the fastest version of
your site that you can have.
We'll also talk about modern tooling and frameworks like Remix which help make your site fast with very little effort.

## Notes

Publicité pour Remix, qui permettrait d'avoir "le meilleur des 2 mondes" entre le MPA et le SPA (multiple/single pages
application) :

* Fast initial content
* Less waterfalls
* Prefetch
* Client side navigation
* Write UI logic once
* Only build necessary UI
* JS doesn't reset between pages