# 11:20 - [Summit Track] Principles for Scaling Frontend Application Development

Malte UBL (Vercel, USA) <[@cramforce](https://twitter.com/cramforce)>

## Abstract

> After spending over a decade at Google, and now as the CTO of Vercel, Malte Ubl is no stranger to being responsible
for
a team’s software infrastructure.
However, being in charge of defining how people write software, and in turn, building the infrastructure that they’re
using to write said software, presents significant challenges.
This presentation by Malte Ubl will uncover the guiding principles to leading a large software infrastructure.

## Notes

"Iteration velocity solves all known problems"

- Principe 1 : Tear down the barriers
- Principe 2 : Make it easy to delete code (une bonne raison de découper le CSS par composant et de le mettre dans le
  même dossier que celui-ci)
- Principe 3 : Migrate incrementally ("There are only 2 types of migrations: incremental migrations and failed
  migrations")
- Principe 4 : Always get better, never get worse
- Principe 5 : Embrace lack of knowledge
- Principe 6 : Eliminate systematic complexity