# 16:20 - [Summit Track] Open Source Awards Ceremony

## Breakthrough of the year

- *Qwik* (framework JavaScript)
- *fresh* (framework JavaScript)
- *Router* (?)
- 🏆 *Next.js App Router*

## The most exciting use of technology

- *Million.js* (DOM virtuel alternatif pour React)
- *Signals* (_state manager_ pour React)
- 🏆 *Tamagui* (framework d'UI qui unifie React et React Native)
- *react-three-offscreen*

## Productivity booster

- *Nx* (système de build)
- *shadcn/ui* (framework d'UI)
- *create-t3-app* (permet de démarrer un projet Next.js)
- 🏆 *Expo Router* (router pour les applications mobiles)
- *Vitest* (framework de test)

## Fun side project of the year

- *Alma* (?)
- *Drift* (?)
- *roomGPT.io*
- 🏆 *React Native Vision Camera*

## The most impactful contribution to the community

- 🏆 *This Week in React* (newsletter sur React)
- *Jack Herrington* (YouTuber)
- *TanStack* (ensemble de libs pour le développement web)
- *Theo - t3.gg* (YouTuber et développeur des outils `t3`)
- *React.dev*