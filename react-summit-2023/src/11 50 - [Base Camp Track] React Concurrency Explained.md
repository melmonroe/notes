# 11:50 - [Base Camp Track] React Concurrency, Explained

Ivan AKULOV (Google Developer Expert, PerfPerfPerf, Pays-Bas) <[@iamakulov](https://twitter.com/iamakulov)>

## Abstract

> React 18! Concurrent features!
You might’ve already tried the new APIs like useTransition, or you might’ve just heard of them.
But do you know how React 18 achieves the performance wins it brings with itself?
In this talk, let’s peek under the hood of React 18’s performance features:
> 
> - How React 18 lowers the time your page stays frozen (aka TBT)
> - What exactly happens in the main thread when you run useTransition()
> - What’s the catch with the improvements (there’s no free cake!), and why Vue.js and Preact straight refused to ship
>  anything similar

## Notes

(La salle était complète, j'ai pas pu y assister)