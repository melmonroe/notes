# 09:20 - [Summit Track] Reactivity: There and Back Again (Keynote)

Michel WESTSTRATE (Meta, Royaume-Uni) <[@mweststrate](https://twitter.com/mweststrate)>

## Abstract

> Everything old is new again.
But this time it's different, promise!

## Notes

- "Not sure to hate or pity AI"
- "Everything old is news again" : la mode est cyclique.
    - Avant on avait du front généré par les backends, on est allés vers du front généré côté navigateur, pour revenir
      aujourd'hui à du front généré côté backend...
    - Même pour la mutabilité/immutabilité et les observables
- On ne tourne pas en rond, on spirale vers le mieux
    - Les dogmes tuent l'innovation

![keynote1.jpg](../img/keynote1.jpg){width=500px}
![keynote2.jpg](../img/keynote2.jpg){width=500px}

* Les **decorators** vont faire leur retour dans React

![keynote3.jpg](../img/keynote3.jpg){width=500px}