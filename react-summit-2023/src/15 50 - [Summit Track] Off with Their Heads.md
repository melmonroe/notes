# 15:50 - [Summit Track] Off with Their Heads: Rise of the Headless Components

Omry NACHMAN (Codux, Israël) <[@OmryNach](https://twitter.com/OmryNach)>

## Abstract

> Aren't You Tired of Repeating Yourself?
Tired of repeating the same code over and over again in your React projects?
In this talk, we'll discover the power of headless components, a design pattern that separates the logic from the
presentation layer, allowing you to create reusable and flexible UI components.
>
> We'll explore how headless components can simplify your development process, saving you both time and effort.
We'll examine popular headless component libraries and provide tips for integrating them into your projects.
Whether you're a beginner or an experienced developer, join us to discover how headless components can help you
streamline your React development and create high-quality, customizable UIs.


## Notes

L'intervenant nous présente différentes libraires d'UI "headless", c'est-à-dire qui n'incluent pas de CSS :
`React Aria`, `TanStack`...

Intérêts :

- Permettent d'avoir un design vraiment unique
- Moins de réécriture si on veut changer de framework

Contrecoups :

- Plus de travail
- Peu de composants avancés disponibles pour l'instant (galeries, carousels, multi-selects...)