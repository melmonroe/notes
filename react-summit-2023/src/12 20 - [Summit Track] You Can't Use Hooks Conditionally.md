# 12:20 - [Summit Track] You Can't Use Hooks Conditionally... or Can You?

Charlotte ISAMBERT (Bam.tech, France) <[@c_isambert](https://twitter.com/c_isambert)>


## Abstract

> It’s the hooks rule number one: “Only call hooks at the top level”.
>
> But what if I told you that this rule does not apply to every hook?
One of them can actually safely be used conditionally.
>
> To understand how useContext is different from other hooks, and why it is exempted from this major rule, we need to know
why this rule exists in the first place.
We will build our own mental model of react’s rendering behavior, focused on how react keeps track of data at runtime:
props, states, refs… and context values.

## Notes

Il y a 2 règles avec les hooks :

- Ils ne peuvent être appelés que dans des fonctions React
- Ils ne peuvent être appelés que _top level_

Un nouveau hook a été proposé par les développeurs React : le hook `use`.
Il aurait la particularité de pouvoir être appelé de façon conditionnelle :

```typescript jsx
const MyComponent = ({condition}) => {
    let data = null;
    if (condition) {
        data = use(promise);
    }
     return <p>{data}</p>;
}
```

Les hooks déjà existants ne peuvent pas être appelés de façon conditionnelle car ils sont stockés sous forme de liste
chaînée (c'est l'implémentation qui été décidée) :

```typescript jsx
const App = () => {
    const [name, setName] = useState("Rachel");
    const [age, setAge] = useState(29);
    
    return (
        <>
            <Greetings name={name}/>
            <p>You are {age} years old</>
            <button onClick={() => setName("Chandler")}>
                Change user name
            </button>
        </>
    );

};

const Greetings = ({name}) => (
    <p>Hi {name}!</p>
);
```

![hooks1.jpg](../img/hooks1.jpg){width=500px}

Il existe en fait déjà un hook qui peut être utilisé de façon conditionnelle : `useContext`.
Puisqu'il est différent, il est stocké en dehors de la liste chaînée et indépendant des autres hooks :

```typescript jsx
const AuthContext = createContext({name: "Joey"});

const App = () => {
const [name, setName] = useState("Joey");
const [age, setAge] = useState(29);

    return (
        <>
            <AuthContext.Provider value={{name}}>
                <Greetings name={name}/>
            </AuthContext.Provider>
            <p>You are {age} years old</>
            <button onClick={() => setName("Ross")}>
                Change user name
            </button>
        </>
    );

};

const Greetings = () => {
    const {name} = useContext(AuthContext);
    return <p>Hi {name}!</p>
};

```

![hooks2.jpg](../img/hooks2.jpg){width=500px}


> Ce n'est pas parce que c'est possible qu'il faut le faire...