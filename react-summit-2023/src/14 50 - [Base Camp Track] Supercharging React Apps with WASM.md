# 14:50 - [Base Camp Track] Supercharging React Apps with WASM

Mukkund SUNJII (ORTEC, Pays-Bas)

## Abstract

> WASM has taken over the web-development scene in the past few years.
It is a language that can be run by the web platform alongside with Javascript.
Being treated as a target language, a variety of low-level, statically-typed languages such as C++ and Rust can be
compiled to WASM.
Thus, a variety of complex, computationally intense applications can now be tackled through readily available web
applications.
Demos of 2 such applications are shown in the presentation and a side-to-side comparison is done next to JS code.


## Notes

(La conférence ne commençait pas à cause de problèmes techniques sur le stream, j'en ai profité pour aller manger avant
le rush du coffee break)
