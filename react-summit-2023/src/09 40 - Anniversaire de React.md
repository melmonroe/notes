# Anniversaire de React 🎉

On a chanté joyeux anniversaire à React, qui fête ses 10 ans cette année !

(En fait ça 10 ans que React est open source : il était déjà utilisé en interne chez Facebook en 2011, puis chez
Instagram en 2012, avant d'être open-sourcé en 2013)

![keynote_twitter1.jpg](../img/keynote_twitter1.jpg){width=500px}
![keynote_twitter2.jpg](../img/keynote_twitter2.jpg){width=250px}

(photos volées sur Twitter)