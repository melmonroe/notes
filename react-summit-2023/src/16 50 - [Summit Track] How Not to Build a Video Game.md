# 16:50 - [Summit Track] How Not to Build a Video Game

Christoph NAKAZAWA (CEO de Nakazawa Tech KK, Japon) <[@cpojer](https://twitter.com/cpojer)>

## Abstract

> In this talk we'll delve into the art of creating something meaningful and fulfilling.
Through the lens of my own journey of rediscovering my passion for coding and building a video game from the ground up
with JavaScript and React, we will explore the trade-offs between easy solutions and fast performance.
You will gain valuable insights into rapid prototyping, test infrastructure, and a range of CSS tricks that can be
applied to both game development and your day-to-day work.

## Notes

Présentation de la stack technique d'_Athena Crisis_, un TRPG codé uniquement en React et en CSS (pas encore sorti, on
peut le wishlister sur Steam).

![athena1.jpg](../img/athena1.jpg){width=500px}
![athena3.jpg](../img/athena3.jpg){width=500px}

- Tout l'affichage est fait en CSS, y compris le brouillard de guerre.
  Pour éviter que des petits malins modifient le CSS et voient des zones qu'ils ne sont pas censés pouvoir voir, il y a
  des vérifications côté backend.

![athena5.jpg](../img/athena5.jpg){width=500px}

 -En plus des tests unitaires classiques, il fait du *_snapshot testing_* pour tester des scénarios (il va tester
  qu'après avoir appuyé sur telle suite de touches, l'image obtenue à l'écran est bien celle qui était attendue).
- Fun fact : sur un clavier virtuel, la taille des touches qui sont affichées à l'écran ne correspond pas à la hitbox
  réelle des touches.
  Celle-ci varie en fonction du mot qu'on est en train de taper (c'est pour ça que parfois on à l'impression d'avoir
  tapé sur la mauvaise touche mais que c'est quand même le bon caractère qui s'affiche).
  - De la même façon, il a fait en sorte que les zones _touchable_ soient en réalité plus grandes que le carré de la
  grille affichée à l'écran.

![athena4.jpg](../img/athena4.jpg){width=500px}

- Pendant le développement, il est indispensable de vérifier les FPS (il y a des libs pour ça)
- La présentation était faite avec ReMDX, une extension de Markdown qui permet d'inclure du React (également développée
  par l'intervenant)

![athena2.jpg](../img/athena2.jpg){width=500px}

- Inconvénient de cette stack : le jeu ne peut être joué que sur PC et mobile, on ne peut pas envisager par exemple de
  sortie Switch