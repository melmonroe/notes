# 10:20 - [Summit Track] Debugging JS

Mark Erikson (Replay.io, USA) <[@acemarke](https://twitter.com/acemarke)>

## Abstract

> As developers, we spend much of our time debugging apps - often code we didn't even write.
Sadly, few developers have ever been taught how to approach debugging - it's something most of us learn through painful experience.
The good news is you _can_ learn how to debug effectively, and there's several key techniques and tools you can use for debugging JS and React apps.

## Notes

> Everyone knows that debugging is twice as hard as writing a program in the first place.
So if you're as clever as you can be when you write it, how will you ever debug it?

-- Brian KERNIGHAN

Après quelques banalités sur la nécessité d'écrire du code propre et documenté, publicité pour Replay :

* Navigateur forké de Firefox / Chrome / Node
* Cliquer sur "Record", utiliser notre application, et reproduire le bug
* Cliquer sur "Save" et l'enregistrement est envoyé sur le cloud
* Se logguer sur Replay et utiliser Replay DevTools pour débugguer à n'importer quel point dans l'enregistrement
* Gratuit pour les projets open source et les projets persos, payant pour les entreprises

![replay1.jpg](../img/replay1.jpg){width=500px}