# 09:50 -  [Summit Track] Improving Developer Happiness with AI

Senna PARSA (GitHub, Pays-Bas)

## Abstract

> GitHub Copilot is an AI pair programmer that can help you write code faster and spend less time writing repetitive
code.This session will cover some interesting use cases for Copilot that could shine a light on its possibilities.
This ranges from prompting Copilot to suggest a function based on a comment, learning how to use a new framework,
tackling a security or accessibility bug, better documenting your code, translating code from one language to another,
etc.
>
> Agenda:
>
> - Introduction to CoPilot
> - What is Copilot
> - How can you use it
> - How it can help you write code faster
> - Copilot Labs experimental features
>
> I will pick examples from the React ecosystem and show how we can fix Security Vulnerabilities and Accessibility issues in some components.

## Notes

Quand on demande aux dévs, on se rend compte que "être productif" = "passer une bonne journée".

Le "framework" SPACE définit 5 métriques pour mesurer la productivité d'un développeur :

- Satisfaction et bien-être
- Performance
- Activité
- Communication et collaboration
- Efficacité et _flow_

L'utilisation des IA permet de rendre les développeurs plus productifs :

![ai1.jpg](../img/ai1.jpg){width=500px}

Les IA sont justes de nouveaux outils qui deviendront plus tard le quotidien des dévs, tout comme les IDE :

![ai2.jpg](../img/ai2.jpg){width=500px}

Il y a malgré tout quelques inconvénients :

- TL;DR : être critique des réponses apportées par les IA
- Le code complexe ou la logique métier ne peuvent être écrits par des IA
- Les suggestions peuvent ne plus être au goût du jour
- Ne remplacent pas la qualité du code et le _security scanning_
- Il reste important de comprendre le code qu'on écrit