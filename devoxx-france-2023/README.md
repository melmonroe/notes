# Devoxx France 2023

Du mercredi 12 au vendredi 14/04/2023, Palais des Congrès de Paris.

## TL;DR

* **Kubernetes** est la techno la plus populaire du moment avec pas moins de 10 présentations à son sujet
* Les pratiques **GitOps** reviennent très souvent
* Retour en force des bases de données relationnelles et en particulier de **PostgreSQL**
* Évidemment beaucoup de sujets sur l'**IA** (ChatGPT, GitHub Copilot, l’avenir du métier de développeur...)
* Pas mal de références au **DDD** (Domain Driven Design)
* **Playwright**, le nouveau framework de test end-to-end de Microsoft, a l’air absolument incroyable 💘💘💘
* Beaucoup de travaux en cours sur **Java** pour rendre le langage plus moderne et plus **performant** (projets Loom,
  Amber, Valhalla...)
* **Grafana Loki** ("comme Prometheus mais pour les logs")
* **VSCode** toujours aussi populaire, c’est ce que la majorité des gens utilisaient pour prendre des notes
* 3816 visiteurs uniques cette année (exposants et speakers compris)
* La prochaine édition aura lieu du 17 au 19/04/2024 ; un deuxième étage sera ajouté afin de pouvoir accueillir 1300
  personnes en plus (pour un total de 4200 conférenciers)

## Mercredi

[09:30 - [University] Value types et Pattern matching : 1 partout, données au centre](src/mercredi/09%2030%20-%20%5BUniversity%5D%20Value%20types%20et%20Pattern%20matching.md)

[13:30 - [University] SQL (le retour) : Démystifions les idées préconçues et utilisons la DB efficacement](src/mercredi/13%2030%20-%20%5BUniversity%5D%20SQL%20%28le%20retour%29.md)

[17:00 - [Tools in Action] Comment être bien onboardée en tant que développeuse junior reconvertie ??](src/mercredi/17%2000%20-%20%5BTools%20in%20Action%5D%20Comment%20etre%20bien%20onboardee.md)

[17:00 - [Tools in Action] Détectez et corrigez automatiquement les problèmes les plus fréquents avec Apache Kafka](src/mercredi/17%2000%20-%20%5BTools%20in%20Action%5D%20Les%20problemes%20les%20plus%20frequents%20avec%20Kafka.md)

[17:45 - [Tools in Action] Playwright : l'outil qui va révolutionner les tests end-to-end](src/mercredi/17%2045%20-%20%5BTools%20in%20Action%5D%20Playwright.md)

## Jeudi

[11:45 - [Conférence] Ctrl+Alt+Depression](src/jeudi/11%2045%20-%20%5BConference%5D%20Ctrl%2BAlt%2BDepression.md)

[13:00 - [Quickie] Avoir un journal de codeur](src/jeudi/13%2000%20-%20%5BQuickie%5D%20Avoir%20un%20journal%20de%20codeur.md)

## Vendredi

[09:00 - [Keynote] Soyons optimistes](src/vendredi/09%2000%20-%20%5BKeynote%5D%20Soyons%20optimistes.md)

[10:45 - [Conférence] Biais & Balivernes](src/vendredi/10%2045%20-%20%5BConference%5D%20Biais%20et%20Balivernes.md)

[10:45 - [Conférence] Qualité radicale - de Toyota à la tech](src/vendredi/10%2045%20-%20%5BConference%5D%20Qualite%20radicale%20-%20de%20Toyota%20a%20la%20tech.md)

[11:45 - [Conférence] Éviter les biais dans la conception de logiciels](src/vendredi/11%2045%20-%20%5BConference%5D%20Eviter%20les%20biais%20dans%20la%20conception%20de%20logiciels.md)

[13:00 - [Quickie] Comment garder son job, quoi qu'il en coûte](src/vendredi/13%2000%20-%20%5BQuickie%5D%20Comment%20garder%20son%20job.md)

[14 30 - [Conference] Le Craft : des concepts au déploiement à l'échelle](src/vendredi/14%2030%20-%20%5BConference%5D%20Le%20Craft%20des%20concepts%20au%20deploiement%20a%20l%27echelle.md)

[15:30 - [Conférence] Voyage au centre de la Veille : Apprendre en continu avec sa veille technologique](src/vendredi/15%2030%20-%20%5BConf%C3%A9rence%5D%20Voyage%20au%20centre%20de%20la%20Veille.md)
