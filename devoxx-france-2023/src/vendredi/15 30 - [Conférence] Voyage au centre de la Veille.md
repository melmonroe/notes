# [Conférence] Voyage au centre de la Veille : Apprendre en continu avec sa veille technologique

https://cfp.devoxx.fr/2023/talk/VBI-2968/Voyage__au_centre_de_la_Veille_:_Apprendre_en_continu_avec_sa_veille_technologique

Vendredi 14/04/23 15h30-16h15 - Amphi Bleu

Fabien HIEGEL (Shodo Nantes) [@fhiegel](https://twitter.com/fhiegel) - David FRANCK (Shodo
Nantes) [@dayoud](https://twitter.com/dayoud)

## Abstract

> React 32 ? Groovy 12 ? Kotlin 7 ? Docker 202 ? Des technologies inconnues déferlent chaque jour des sombres recoins de
> l’internet.
> Le temps de vous y intéresser est difficile à trouver : ni votre projet, ni votre organisation personnelle ne le
> permettent.
> Et finalement, quand arrive le moment de s’y atteler, vous manquez d’efficacité.
>
> Vous aimeriez entretenir vos connaissances et monter régulièrement votre niveau sur votre domaine d’expertise.
> Ou encore, satisfaire votre curiosité en découvrant de nouvelles compétences.
> Un monde s’ouvre à vous avec une myriade de possibilités, mais comment savoir par quel bout commencer ?
>
> Alors, acceptez la quête de votre veille personnelle et partons à la recherche du livre de la connaissance.
> Découvrons un processus et des outils pour transformer votre veille.
> Vous aurez alors les clés pour mettre en place votre propre contenu utile et exploitable pour aller plus loin dans
> votre
> quotidien.
> Le résultat : un second cerveau qui vous accompagnera toute votre vie.

## Notes

Pourquoi faire de la veille ?
Quatre objectifs :

* **Formation** : devenir opérationnel
* **Efficacité** : améliorer sa productivité
* **Diversité** : prendre de la hauteur
* **Employabilité** : être à jour pour le marché

Quand faire de la veille ?

* Si ce qu’on apprend va servir à notre employeur, sur le temps pro
* Sinon, temps perso
* Se réserver un créneau spécifique (par exemple 30 minutes tous les vendredis matins)

Comment se créer une habitude ?

* Signal : doit être évident
* Besoin : doit être attractif
* Action : doit être facile
* Récompense : doit être plaisant

Comment faire de la veille ?
Trois étapes : sourcer, traiter, valoriser.

* **Sourcer**
    * Suivre des sites généralistes, des podcasts, des gens sur Twitter, sur LinkedIn...
    * Choisir des ressources : un article de blog, un épisode de podcast...
        * Les lister (Pocket, Notion, Trello, Notes, Evernote...)
        * Attention au risque d’accumulation
    * Prendre des notes sur sa veille, sinon c’est de la perte de temps
        * Permet de mieux mémoriser et d’apporter une réflexion
        * Paraphraser/Reformuler, ne pas copier/coller - ce sera plus facile à relire, et on est sur d’avoir compris
        * Plein de façons de noter : papier, numérique, mindmap, sketchnote, écrit, vocal... Chacune a ses avantages et
          ses inconvénients

* **Traiter**
    * Retravailler ses notes le plus rapidement possible
    * _Zettelkasten_, boîte de notes
        * Créer des petites notes, des notes atomiques
        * Refactoriser régulièrement ses notes
        * Créer des connexions entre les notes
        * Typer ses notes

![voyage-veille01.jpeg](../../img/voyage-veille01.jpg){width=500px}

* **Valoriser** : plein de façons de valoriser sa veille
    * Apprendre
    * Innover
    * Expérimenter
    * Partager (discuter, enseigner)

Quelques idées supplémentaires :

* **Good morning learning** (30 minutes) : tous les matins, chacun lit le même article pendant 20 minutes puis on
  échange en groupe pendant 10 minutes
* **Xtrem reading** (1 heure) : chacun prend un livre et le lit pendant 20 minutes.
  Au bout des 20 minutes, on échange son livre avec quelqu’un d’autre.
  On lit cet autre livre pendant 20 minutes.
  Ensuite, on échange en groupe pendant 15 minutes
* **Mob programming** : on met tout le monde sur un même sujet, dans la même pièce, et on partage tout

Recommandations

* Lectures
    * Atomic Habits
    * How To Take Smart Notes
* Applications
    * https://noteapp.info pour les comparer
    * Obsidian
    * Notion
    * Evernote
    * Joplin
