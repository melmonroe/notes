# [Conférence] Le Craft : des concepts au déploiement à l'échelle

https://cfp.devoxx.fr/2023/talk/FMU-6426/Le_Craft_:_des_concepts_au_deploiement_a_l’echelle

Vendredi 14/04/23 14h30-15h15 - Salle Maillot

Mattieu VINCENT (Sopra Steria) [@yadamad03](https://twitter.com/yadamad03) - Guillaume LE DAIN (Sopra
Steria) [@gledain](https://twitter.com/gledain)

## Abstract

> La qualité des développements a trop longtemps été mis de côté pour des raisons budgétaire ou encore de timing.
> Beaucoup de projets ne sont pas arrivés au bout à cause de ça ou pire doivent être refondu 2 ans plus tard parce qu’ils
> deviennent impossible (ou trop couteux) à faire évoluer.
>
> Une des réponses à ces problème est le craftsmanship qui est arrivé en 2009. Comment professionnaliser les
> développement et mettre la qualité au centre de nos priorités.
> Ce n’est que depuis peu qu’on le voit appliquer à grande échelle sur de gros projets et même à échelle d’une entreprise.
>
> Dans ce talk, nous aborderons :
>
> * les concepts rapidement (TDD, Clean code, SOLID, ...)
> * comment le mettre en oeuvre à l’échelle de quelques projets pour définir les bases des bonnes pratiques
> * comment nous le déployons à l’échelle d’une entreprise de plusieurs milliers de personnes (méthodologies,
    formations, ...)
>
> Avec ce talk, nous vous partagerons les bonnes pratiques et méthodologies que nous avons mis en place au sein de notre
> entreprise pour le déployer le plus largement possible

## Notes

> Publicité pour Volcamp, une conférence tech qui se déroule en Auvergne.

* Avant : Cycle en V
* 2001 : Agilité
* 2009 : Craftmanship

3 des grands principes du manifeste du craftmanship :

* **Partenariat** : co-construire avec les équipes clients
* **Code de qualité** : bonne pratiques etc
* **Recherche constante d’amélioration** : fédérer les équipes, impliquer les dévs, répartir les responsabilités

Comment mettre ça en place ?

* Étape 1 : **convaincre le client**

> Bus factor: 
Le nombre de personnes qui peuvent passer sous un bus avant que le projet soit un échec.

* Étape 2 : **acculturer le client**.
  Quand ça devient tendu sur le projet, 3 réactions possibles :
    * détendre le calendrier
    * mettre plus de gens
    * rogner sur la qualité : si ça doit arriver, contractualiser avec le client le fait qu’il faudra régler la dette
      technique une fois la vague passée

* Étape 3 : **embarquer les équipes**.
  Chez Sopra, ils ont mis en place les 4S
    * Session refresh : toutes les semaines, aborder avec les équipes les sujets et difficultés rencontrées au quotidien
    * Smart refresh : tous les mois, descente d’informations (veille techno, REX...)
    * Skill refresh : 2 fois par mois, kata etc
    * Squad refresh : responsabilisation, référent tournant par techno (~3 mois)

Niveau RH :

* Accompagnement de carrière
* Parcours de formations
* Parcours de formation "formateur"
* Mentorat
