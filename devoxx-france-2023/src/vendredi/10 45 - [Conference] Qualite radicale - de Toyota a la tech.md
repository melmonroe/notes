# [Conférence] Qualité radicale - de Toyota à la tech

https://cfp.devoxx.fr/2023/talk/JMK-9218/Qualite_radicale_-_de_Toyota_a_la_tech

Vendredi 14/04/23 10h45-11h30 - Salle Paris 241

Flavian HAUTBOIS (Build to Sell) [@flavianhautbois](https://twitter.com/flavianhautbois) - Woody ROUSSEAU (Sipios) [@woodyrousseau](https://twitter.com/woodyrousseau)

## Abstract

> Il y a du chemin... Là où dans l’industrie les défauts se comptent en défaut par million de pièces produites, un
> développeur introduit en moyenne 70 bugs pour 1000 lignes de code produites.
> Nous nous sommes plongé dans les expérimentations de Sadao Nomura, qui a lancé dans des usines Toyota le Dantotsu, "
> Better than the best" un programme sur 3 ans capable de réduire de 85% les défauts.
>
> Nous nous sommes inspiré dans la tech des pratiques, management visuels et outils du Dantotsu pour :
>
> Éradiquer les causes profondes d’un bug en 24h après sa détection Identifier les "weak points", problèmes types qui
> nécessitent de muscler le système de formation Créer une culture de la qualité où chacun partage ses bugs résolus

## Notes

* Avoir 0 bugs n’est pas une fin en soi, mais une étoile à suivre et la meilleure stratégie pour avoir des équipes tech
  exceptionnelles

![toyota_p02.jpeg](../../img/toyota_p02.jpeg){width=500px}

* Idée reçue : la qualité n’est pas moins chère que la non-qualité

![toyota_p04.jpeg](../../img/toyota_p04.jpeg){width=500px}

* Sadao NOMURA, employé de Toyota, a inventé il y a 20 ans une méthode pour faire de la _qualité radicale_ (ouvrage : "
  The Toyota way of dantotsu radical quality improvement")
    * Il a permis à tous les ateliers Toyota où la méthode a été appliquée de réduire de 88% les défauts en 3 ans

* Il classifie les défauts en 4 types
    * A : défaut qu’on repère tout seul pendant qu’on développe (par exemple quelqu’un qui écorche un phare sur la
      chaîne de production)
    * B : défaut qui arrive un peu plus loin (par exemple dans la CI/CD)
    * C : défaut qu’on sort et qu’on envoie aux intermédiaires/prestataires
    * D : défaut qui impacte l’utilisateur / le client final (exemple : accident de voiture)

![toyota_p14.jpeg](../../img/toyota_p14.jpeg){width=500px}

* On peut découper différemment, ajouter/enlever des lettres pour s’adapter à notre entreprise
* Il définit ensuite 8 étapes à mettre dans place dès qu’on trouve un défaut (à faire en 24 heures, pour tous les
  défauts, quelque soit leur gravité)

![toyota_p17.jpeg](../../img/toyota_p17.jpeg){width=500px}

* On peut définir les types A à D dans un flux de développement

![toyota_p21.jpeg](../../img/toyota_p21.jpeg){width=500px}

* Et adapter les étapes de résolution, en modifiant un peu les délais

![toyota_p24.jpeg](../../img/toyota_p24.jpeg){width=500px}

* D’autres choses qu’on peut appliquer à la tech :
  * Quantifier, se fixer des objets en utilisant des métriques (par exemple les métriques DORA)
  * Management visuel (Jira)
  * Standards de développement : écrire les choses à vérifier, les bonnes pratiques... qui serviront de support de
  formation pour les nouveaux arrivants
  * Formation : tutoriels, katas, pair/mob programming, code review...
  * Weak point management : repérer les problèmes récurrents (par exemple si on rencontre souvent des problèmes avec la
  base de données, on peut monter une taskforce pour investiguer)
  * Environnement de travail : lint/codestyle, structure du code, portail développeur, sytème de gestion des
  connaissances, rituels...
