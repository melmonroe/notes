# [Conférence] Biais & Balivernes

https://cfp.devoxx.fr/2023/talk/MBR-7822/Biais_&_Balivernes

Vendredi 14/04/23 10h45-11h30 - Salle Paris 242 AB

Thomas C. DURAND (La Tronche en Biais) [@acermendax](https://twitter.com/acermendax)

## Abstract

> Pourquoi certaines idées nous attrapent-elles ?
>
> Comment des gens normaux aboutissent à des croyances qui nous semblent folles ?
>
> Le cerveau humain glisse le long de pentes naturelles qui nous évitent de commettre des erreurs graves de
> conséquences.
>
> Cela se réalise au prix d’une forme de cécité à des erreurs moins violentes mais qui, à la longue, peuvent nous porter
> des préjudices sévères.

## Notes

**Cerveau** = "appareil avec lequel nous pensons que nous pensons" (Ambrose Bierce) : machine archaïque.

* Intuitions fortes et immédiates
* Prégnance des émotions
* Illusion de percéption
* Illusions logiques
  * Estime de soi vs réalité
* Influences et désirabilités sociales
* Gérer les angoisses
  * **Intelligence** != rationnalité (le rationnel veut le savoir si il a tort)

### Biais

Illusions cognivites / **biais cognitifs** (erreurs du cerveau).

* **Biais de confirmation** : on a tendance chercher / plus aimer les infos qui confirment ce qu’on pensait déjà
* **Biais du survivant** (par exemple les gens qui sont guéris d’un cancer par un naturopathe - les autres sont plus là
  pour témoigner)
* **Effet spectacteur** (ou ueffet témoin) : il vaut mieux se faire agresser quand il y a pas trop de monde
* **Cascade d’engagement** : plus on défend quelqu’un plus on y croit
* Effet de simple exposition
* Fixité fonctionnelle
* Erreur fondamentale d’attribution : on pense que ce qui arrive aux gens c’est grâce à leurs qualités/défauts.
  * Elon Musk n’est pas devenu riche par qu’il est intelligent, il a hérité de mines d’émeraude.
  On croit au self-made man parce qu’on se dit que nous aussi peut y arriver.
  * On croit que nos réussites sont dûes à nos qualités mais que nos échecs sont dûs au contexte
* Effet Drunning-Kruger (effet de surconfiance) : "les cons sont trop cons pour réaliser qu’ils sont cons"
* Reconnaissance de formes invisibles

### **Balivernes**

"Récits contagieux qui cherchent à encapsuler une explication du monde dans une narration déconnectée du réel, mais
parfaitement adaptées aux penstes naturelles de l’esprit humain" (Mendax)

Les 4 principes de la baliverne (selon Mendax) :

* **Le principe narratif** : ça fait une belle histoire ("les pyramides n’ont pas été construites par les égyptiens...")
* **Le principe d’attraction** : si la vérité c’est qu’on va dans le mur niveau écologie, on veut pas le savoir et on
  préfère une théorie qui nous attire plus
* **Le principe de résilience** : "Il s’est passé ça ?
  C’est la preuve qu’il y a un complot.
  Il ne s’est pas passé ça ?
  C’est aussi un complot."
* **Le principe d’asymétrie** : Asymétrie de Brandolini - il faut beaucoup plus de ressources pour réfuter une baliverne
  que pour l’énoncer
