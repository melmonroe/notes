# [Quickie] Comment garder son job, quoi qu'il en coûte

https://cfp.devoxx.fr/2023/talk/HOR-8423/Comment_garder_son_job,_quoi_qu’il_en_coute

Vendredi 14/04/23 13h00-13h15 - Salle Neuilly 252 AB

Raphael BLEMUS (Criteo)

## Abstract

> D’après le principe de Peter « dans une hiérarchie, tout employé a tendance à s’élever à son niveau d’incompétence ».
> En me basant sur le livre Simple Sabotage Field Manual, publié par le United States Office of Strategic Services (OSS,
> aujourd’hui la Central Intelligence Agency) en 1944 ainsi que d’autres papiers sur la toxicité au travail.
> Je vais donc vous apprendre comment vous pourrez garder votre travail le plus longtemps possible grâce à un ensemble de
> compétences et de techniques/biais cognitifs afin de vous rendre irremplaçable, ou de vous permettre de débusquer des
> personnes usant de ces techniques...

## Notes

L’OSS, ancêtre de la CIA, a publié en 1944 "Simple sabotage - Field manual", un livre qui explique comment ralentir des
entreprises dans un pays ennemi, en étant consciemment nul et toxique.
Il a été déclassifié en 2008.

* Principe de Peter : dans une hiérarchie, tout employé à tendance à s’élever à son niveau d’incompétence
* Ralentir les autres
    * Toujours suivre l’administration
    * Nommer des gens incompétents
    * Créer de la confusion en créant des réunions avec beaucoup de gens
    * Propager de fausses informations
    * Revenir sur des décisions déjà prises
* Plomber le moral
* _Dungeon Master_ : quelqu’un qui a travaillé sur un projet, qui connait tout, qui ne fait plus de dév mais qui est
  indispensable car il veut garder le contrôle
* OKR les plus fous possibles, pas quantifiables (améliorer l’interface, faire *2 sur le nombre d’utilisateurs...)
