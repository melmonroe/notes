# [Keynote] Soyons optimistes

https://cfp.devoxx.fr/2023/talk/RZB-3250/Soyons_optimiste

Vendredi 14/04/23 09h00-09h20 - Amphi Bleu

Thomas C. DURAND (La Tronche en Biais) [@acermendax](https://twitter.com/acermendax)

## Abstract

> Au temps des Fake News et de la Post-vérité, il est facile de se décourager et de penser que les humains sont foutus.
> Mais l’esprit critique réside souvent même là où on pense qu’il est absent.

## Notes

* On va pas parler d’écologie ou de guerre...
* **Optimiste != la positive attitude** (qui n’est qu’un truc de patrons pour faire croire aux employés que s’ils ne
  vont pas bien, c’est de leur faute)
* "Pense par toi même" = **positive attitude = bullshit** : nous pensons avec les autres

* **Biais d’internalité** : on a tendance à croire que nous on agit pour de bonnes raisons (des raisons "internes")
  tandis que les autres non (pour des raisons "externes")
* La propagande est peu efficace pour changer la façon de penser de quelqu’un mais permet plutôt de conforter quelqu’un
  dans des croyances qu’il avait déjà
* **Biais de surconfiance** - surtout chez les hommes : tendance à coire qu’on est plus moral et plus malin que les
  autres (même quand le questionnaire est fait en prison !)
* **Biais d’endogroupe** : on voit les problèmes ailleurs, mais pas chez nous
    * Ce n’est pas un défaut personnel mais plutôt la conséquence de champs de force culturaux et sociaux
    * Si c’était individuel ce serait facile à éliminer, mais comme c’est social il faut des sciences humaines et de la
      politique
* **"Les foules ne sont pas folles"** : c’est rarement un mec dans son coin qui révolutionne le monde mais des paquets
  de gens, on est plus intelligents en groupe (un bon exemple : le projet Wikipédia)
* **Si on perd l’optimisme qui cherchera des solutions** ?
