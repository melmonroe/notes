# [Conférence] Éviter les biais dans la conception de logiciels

https://cfp.devoxx.fr/2023/talk/GIB-5595/Eviter_les_biais_dans_la_conception_de_logiciels

Vendredi 14/04/23 11h45-12h30 - Salle Maillot

Mélanie REVERSAT (Dataiku) - Clémence BIC (Dataiku)

## Abstract

> Comment se fait t’il que les logiciels de recrutement de certains grands noms de la tech aient été tristement célèbres
> pour ne recruter qu’un type particulier de collaborateurs.
> Pourquoi certains logiciels de planification des horaires dans la grande distribution désorganisent la vie de leurs
> employés?
> Pour quelle raison les logiciels de reconnaissance faciale semblent plus efficaces sur certaines catégories de la
> population?
> La réponse est simple, ces logiciels comme beaucoup d’autres sont biaisés, c’est à dire qu’une déviation (ou plusieurs)
> systématique de la pensée logique et rationnelle par rapport à la réalité a été involontairement introduite.
> Dans cette session nous vous proposons de réfléchir à comment ces biais se développent et affectent vos logiciels, pour
> pouvoir mieux les identifier et les contrôler à la fois avec des approches techniques et humaines.
> Nous insisterons particulièrement sur la nécessité de recruter des équipes diversifiées, qui restent la meilleure arme
> contre l’introduction de biais.

## Notes

* **Biais cognitif** = réflexe de pensée faussement logique, inconscient et systématique

* **Système 1** = Pensée rapide / intuitive
* **Système 2** = Pensée lente / analysée

* **Biais d’encrage** : acheter quelque chose dont on a pas besoin parce que c’est en promo
* **Effet de groupe** : ne pas oser aller à l’encontre du consensus
* **Biais de confirmation** : on cherche des infos qui confirment notre propre opinion

### Les logiciels biaisés

* Quelques exemples de logiciels biaisés (IA) :

    * Le logiciel de recrutement d’Amazon, jamais passé en prod, qui sélectionnait en priorié les CV d’homme, car elle
      préférait les mots utilisés par ceux-ci
    * COMPAS (Correctional Offender Management Profiling for Alternative Sanctions) : logiciel américain pour déterminer
      le risque de récivide d’un accusé qui accusait en premier les personnes de couleur

* Pourquoi ces modèles sont biaisés ?
  Ça peut arrriver à toutes les étapes du développement.

    * Données (données non représentatives, biais d’observation)
    * Modélisation de l’algorithme / entraînement - choix des métriques - choix du modèle lui-même

* Comment éviter ça :

    * Réflexion sur la méthode de collecte de données
    * Analyse des données d’entrée
    * Précautions dans le choix des méthodes d’évaluation et d’optimisation du modèle
    * Privilégier des modèles inteprétables

* A posteriori :

    * Analyser l’importance des variables sur les prédictions émises par le modèle
    * _Fairless analysis_ : analyser la "parité démographique", analyser l’égalité des opportunités au sein d’une
      sous-population
    * Monitoring : _drift_ dans les comportements, _drift_ dans les données

### Les biais dans les équipes produit/développement

* Éviter l’effet de groupe / le biais d’ancrage
    * Par exemple en poker planning, tout le monde montre sa carte en même temps pour ne pas être influencé par les
      autres
* Innovation participative : boîte à idées pour que les gens osent parler
* Biais de confirmation et biais de cadrage : selon qui a fait le dev, on teste pas pareil ("machin est nul/trop fort,
  on va beaucoup/pas beaucoup tester")
    * Faire tester par beaucoup de monde pour avoir plein d’idées et penser à tous les cas (diversité)
