# [Quickie] Avoir un journal de codeur

https://cfp.devoxx.fr/2023/talk/LMB-1204/Avoir_un_journal_decodeur

Jeudi 13/04/23 13h00-13h15 - Salle Neuilly 252 AB

Sandrine BANAS (CGI) [@cosJava](https://twitter.com/cosJava)

## Abstract
> Avez-vous déjà perdu du temps à rechercher les mêmes snippets de code ?
L’impression d’avoir déjà résolu le même problème mais oublié comment ?
Ne pas savoir où est passée votre journée de travail ?
Du mal à gérer votre charge de travail ?
L’impression de ne pas contrôler votre carrière et de naviguer dans le brouillard ?
L’envie de devenir un(e) meilleur(e) développeur(se) ?
La réponse c’est d’avoir un journal de codeur(se).
Parlons-en !

## Notes

* Charles Darwin, Léonard de Vinci, Carl Jung, Marie Curie... tenaient un journal
* Enquête du ministère de la Culture : seulement 7% des Français de plus de 15 ans tiennent un journal
* La tenue d’un journal doit avoir un objectif (trouver ses marques dans une nouvelle entreprise, capitaliser ses compétences, suivre sa carrière...)
* Le contenu d’un journal est simple : (date + trace) * nombre de jours
* Plusieurs formats possibles : écrit, numérique, oral...

Plusieurs types de traces :

* Traces **informatives** : toutes les infos du projet, qui, quoi, quelles technos...
  * Consigner ses succès professionnels est utile pour les entretiens annuels et pour l’estime de soi
* **Indicateurs SMART** : Specific, Measurable, Achievable, Relevant, Time-bound
  * Un smiley pour indiquer le bien-être dans l’équipe
  * Une checkbox pour savoir si on a fait sa veille techno ou pas
  * Une échelle de 0 à 10 pour noter l’intérêt technique du moment
* Traces **techniques** : pour/contre d’une techno, bugs résolus...
* **Dessins** (_sketchnotes_)
* Schémas heuristiques (**graphes**)
* Bouts de code
* Pourcentages
* ...
