# [Conférence] Ctrl+Alt+Depression

https://cfp.devoxx.fr/2023/talk/IQF-0273/Ctrl+Alt+Depression

Jeudi 13/04/23 11h45-12h30 - Salle Paris 241

Manon GRUAZ (Haleo) [@manongruaz](https://twitter.com/manongruaz)

## Absract 

> Avez-vous déjà remarqué que Designer, Developpeur et Dépression commencent par les deux mêmes lettres? Coïncidence? Tous les ingrédients pour une bonne dépression sont intrinsèques au monde de la tech: Perfectionnisme, Comparaison, Performance à tout prix. Faites mijoter dans une société où l’on doit à plaire en permanence, où quoi que l’on fasse nous ne sommes jamais assez… BOUM, reBOUM et KaBOUM!
>
> Je m’appelle Manon, mon métier est designer et j’ai traversé une dépression majeure. J’ai été en arrêt de travail pendant 11 mois.
Alors que le burnout commence à devenir acceptable, la dépression ne l’est toujours pas. Le burnout c’est le badge d’honneur des ambitieux, alors que la dépression est encore synonyme de faiblesse. Il faut avoir honte d’être en dépression.
>
> J’ai envie de dire Fuck la honte. J’ai envie de lever le voile sur une réalité que beaucoup trop vivent cachés. Souvent isolé, le retour à la vie « normale » est un chemin semé d’embûches que je comparerais plus à un rollercoaster qu’une croisière dans mon lit. Avec humour, je vais parler sans fard de la solitude mais aussi de l’acceptation, les remises en question, les apprentissages et l’espoir au bout du tunnel.

## Notes

Conférence très touchante sur le burnout et la dépression.
