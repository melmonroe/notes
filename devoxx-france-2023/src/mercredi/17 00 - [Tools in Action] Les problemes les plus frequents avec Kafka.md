# [Tools in Action] Détectez et corrigez automatiquement les problèmes les plus fréquents avec Apache Kafka

https://cfp.devoxx.fr/2023/talk/PQW-5372/Detectez_et_corrigez_automatiquement_les_problemes_les_plus_frequents_avec_Apache_Kafka

Mercredi 12/04/23 17h00-17h30 - Salle Neuilly 252 AB

Florent RAMIÈRE (Conduktor) [@framiere](https://twitter.com/framiere) - Jean-Louis BOUDART (
Conduktor) [@jlboudart](https://twitter.com/jlboudart)

## Abstract

> Kafka est un système super flexible et hyper configurable : c’est autant une bénédiction qu’une malédiction.
>
> Comment pouvons-nous nous assurer que les applications utilisent efficacement les ressources Kafka, sachant qu’une
> majeure partie de la configuration est effectuée côté client... si loin des yeux attentif des chers ops et architectes ?
>
> Parmi les problèmes les plus courants, citons les
>
> * Producteurs dont la taille des batchs, linger est configurée de manière inefficace
> * Producteurs qui n’utilisent pas de compression, ou ont un ack inadéquat
> * Consommateurs un peu trop optimistes
> * Consumer groups avec plus de membres que de partitions
> * Création de topic sans convention de nommage, ou avec trop de partitions
> * Topics qui mélangent involontairement des données avec des schémas...
> * Applications qui ne gèrent pas les erreurs de déserialization
> * Applications qui ne gèrent pas l’idempotence
> * Applications qui ne gèrent pas les rebalance storm
> * etc.
>
> Rejoignez-nous pour discuter de ces problèmes, mais surtout des outils qui peuvent être mis en place pour les tuer...
> net!

## Notes

Présentation publicitaire pour le produit des intervenants, Conduktor Gateway, un outil qui se place entre le serveur
Kafka et les clients pour retravailler les messages.
