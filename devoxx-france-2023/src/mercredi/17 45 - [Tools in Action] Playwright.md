# [Tools in Action] Playwright : l'outil qui va révolutionner les tests end-to-end

https://cfp.devoxx.fr/2023/talk/XLG-7556/Playwright_:_l’outil_qui_va_revolutionner_les_tests_end-to-end

Mercredi 12/04/23 17h45-18h15 - Salle Neuilly 252 AB

Jean-François GREFFIER (
Conserto) [GitHub](https://github.com/jfgreffier/) / [@jfgreffier](https://twitter.com/jfgreffier)

## Abstract

> Certaines équipes hésitent ou ne mettent pas en place de tests end-to-end.
"Trop dur à mettre en place" "Tests difficiles à maintenir" "Perte d’argent et de temps"
>
> Dans cette conférence, je vous propose de partir à la découverte de Playwright.
> Un nouvel outil Microsoft, rapide, fiable et complet, qui va probablement changer vos aprioris et appréhensions sur
> les
> tests sur navigateurs.
>
> À travers live-coding, démos et exemples, nous verrons comment créer des tests robustes.
> Nous évoquerons aussi les performances, les limitations à prendre en compte, l’outillage ou encore comment débugger,
> même après coup.
>
> Venez découvrir le futur des tests end-to-end !

## Notes

> TL;DR : Ça a l’air **vraiment** incroyable.

Créé en 2020 chez Microsoft, par une équipe expérimentée.
Hérité de Puppeteer.

* Permet de piloter 3 navigateurs (Chrome/Edge, Firefox, Safari) avec une API unifiée.

![playwright_01.jpeg](../../img/playwright_01.jpg){width=500px}

* Pas basé sur WebDriver (plus rapide, plus fiable)
* Multi langages : on peut coder les tests en Java, Javascript, Typescript, Python...
* Utilisable avec JUnit, Jest, PyTest...
* Compatible avec des outils comme Cucumber
* Propose des "web first assertions" (dans une lib), par exemple `expect(...).toBeChecked()` pour une checkbox
* On peut chaîner les locateurs `....locator(...).locator(...)....click()`
* Plugin VSCode gratuit, plugin JetBrains payant

### Playwright Test

Playwright Test est le _test-runner_ de Playwright.
Il permet de lancer un navigateur et un contexte.

* L’inspecteur de Playwright Test permet d’explorer le HTML pour trouver le meilleur locateur Playwright possible
* Génération de tests avec CodeGen :
    * On peut directement enregistrer le parcours utilisateur et générer le code qui va bien
    * Le code peut être généré en différents langages (Java, Javascript, Typescript, Python...)
    * On peut visualiser chaque étape du parcours dans l’interface
* Génération de rapports
* Parallélisation des tests :
    * Scaling (plusieurs workers en même temps)
    * Sharding (répartition des tests sur différents jobs dans la CI)

![playwright_02.jpeg](../../img/playwright_02.jpg){width=500px}

* Le debugger permet de voir l’exécution des tests sous forme de timeline et d’accéder à un screenshot du DOM (HTML +
  CSS, pas de Javascript) à chaque étape

![playwright_03.jpeg](../../img/playwright_03.jpg){width=500px}

### Performances

![playwright_04.jpeg](../../img/playwright_04.jpg){width=500px}

### Auto-waiting

Playwright intègre une mécanique automatique d'_auto-waiting_, il est capable d’attendre tout seul qu’un élément soit
présent dans la page.

![playwright_05.jpeg](../../img/playwright_05.jpg){width=500px}

### Points négatifs

* Certaines fonctionnalités ne sont disponibles qu’en Typescript
* Android expérimental
* La philosophie de Playwright étant de supporter uniquement les 3 navigateurs principaux, il n’y a pas de support iOS
* Communauté encore jeune
* Peu de ressources en français
* Plugin JetBrains payant
