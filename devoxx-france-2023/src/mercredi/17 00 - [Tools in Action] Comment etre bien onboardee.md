# [Tools in Action] Comment être bien onboardée en tant que développeuse junior reconvertie ??

https://cfp.devoxx.fr/2023/talk/PWO-4345/Comment_etre_bien_onboardee_en_tant_que_developpeuse_junior_reconvertie_%3F%3F

Mercredi 12/04/23 17h00-17h30 - Salle Neuilly 253

Amélie ABDALLAH (theTribe) [@AlonahAmelie](https://twitter.com/AlonahAmelie)

## Abstract

> En tant que développeuse juniore en reconversion professionnelle (quand celle-ci s’arrête-t-elle exactement ?), j’ai
> dû changer d’entreprise suite à une très mauvaise expérience express dans ma première entreprise.
> De la première à la seconde, ce fut le jour et la nuit.
>
> C’est un sujet qui part sur deux axes principaux :
>
> L’entreprise, qui possède un rôle majeur dans l’onboarding des salarié-e-s, autant techniquement (process, mini
> formations, présentations des pôles si moyenne/grande entreprise) qu’humainement, La développeuse ou le développeur qui
> doit s’intégrer, avec force, autonomie, en gérant la pression de son nouveau métier + nouveau lieu de travail +
> nouvelles responsabilités etc..

## Notes

Tronc commun pour tous les nouveaux arrivants dans l’entreprise :

* Ne pas faire coder dès le premier jour (si on fait ça c’est qu’on a pris le temps de rien expliquer)
* Faire une mise à niveau sur les informations administratives, les processus...
* Rencontre/présentation
    * De tous les métiers (designer, sales, product manager, recruteur, support...)
    * De toutes les équipes
    * Des cofondateurs...
* Mettre en place un système de parrainage
* Faire écrire un rapport d’étonnement (~6 semaines après l’arrivée)

Plus spécifiquement pour les juniors et/ou les reconvertis :

* Créer un repo pour la mise à niveau technique
    * Sandbox sur les langages les plus utilisés dans l’entreprise
    * Documentation sur les architectures utilisées
* Faire en sorte que le mentor soit disponible (questions, pair programming...)
    * Bloquer du temps pour ça
    * Temps priorisé pour la mise à niveau en compétences (et pas pour être productif)
    * Tout ça pendant minimum 2 semaines
* Avoir de l’empathie sur le parcours de la personne reconvertie
    * Prendre en compte ce qui a poussé la personne à se reconvertir (burnout...)
    * Être à l’écoute de ses attentes sur les tâches prévues
    * Valoriser l’ensemble de son parcours pro (par exemple quelqu’un qui a travaillé en call center peut être doué pour
      parler aux gens)
* Avoir envie de prendre des personnes reconverties
    * Différentes personnes avec des parcours différents => des idées plus variées
