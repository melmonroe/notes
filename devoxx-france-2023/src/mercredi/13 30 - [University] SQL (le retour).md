# [University] SQL (le retour) : Démystifions les idées préconçues et utilisons la DB efficacement

https://cfp.devoxx.fr/2023/talk/AOX-3597/SQL_(le_retour):_Demystifions_les_idees_preconcues_et_utilisons_la_DB_efficacement

Mercredi 12/04/23 13h30-16h30 - Salle Paris 242 AB

Franck PACHOT (Yugabyte) [@FranckPachot](https://twitter.com/FranckPachot)

## Abstract

> Après la vague du NoSQL, le SQL fait son retour.
> La guerre est finie : Les bases NoSQL rajoutent des fonctionnalités SQL, et les bases SQL peuvent stocker des
> documents,
> même avec scalabilité horizontale.
> Mais après les combats marketing qui ont déformé la réalité, puis l’abandon du SQL dans les formations développeur, il
> reste beaucoup de doutes, méconnaissances et idées préconçues.
> Ce n’est pas l’idéal pour développer une persistence de données solide et performante.
>
> Nous allons voir les principes de base du SQL, en évitant toute approche académique (pas de formes normales, pas de
> longue théorie sur les niveaux d’isolation).
> Seulement les faits, pour comprendre l’utilité des bases SQL pour un développeur, et leur utilisation efficace.
>
> * Est-ce qu’il faut craindre les jointures et dénormaliser à fond ?
> * Est-ce mal de mettre des UUID partout ?
> * Dois-je dois passer en niveau d’isolation Repeatable Read?
    Ou Serializable?
> * Mon DBA me dit que les ORM sont le mal absolu, mais ils me sauvent la vie en dev, que faire ?
> * Dois-je éviter les procédures stockées à tout prix ?
> * Peut-on debugger un SQL complexe ?
> * Je n’utilise que du SQL standard, mon application est-elle SGBD-agnostique ?

## Notes

La conférence était principalement un rappel des bonnes pratiques à l’usage des dévs.

> TL;DR : Ne pas réinventer la base de données, exploiter ses capacités pour ne pas avoir à le faire dans le code (par
> ex. : utiliser `NOT NULL` pour ne pas avoir à gérer de `NullPointerException`)

* Ne pas faire de requêtes à bases de concaténations de chaînes de caractères (utiliser Hibernate, etc)
* Avec Hibernate :
    * Vérifier qu’il ne fait pas de  `SELECT *`
    * Vérifier que, quand on veut mettre à jour une colonne, il ne met pas à jour toute la ligne
* Faire tout ce qui est `SORT` et `GROUP BY` via la BDD
* Vérifier les plans d’exécution de toutes les requêtes
* Utiliser des bind variables

> Exception : Si on veut faire des choses vraiment différentes en fonction d’un paramètre (par exemple en fonction d’une
> colonne
_type_) ou si on a des suppressions logiques (colonne _is_deleted_), il vaut mieux utiliser des litterals.
> Sinon, le plan exécution déterminé à la compilation ne sera optimisé que pour un des deux chemins.

### Isolation Level

* `Par défaut` Dépend de la base de données
* `Serializable` Un seul utilisateur peut faire la même action en même temps, sinon la BDD lance une erreur qui doit
  être
  gérée par l’appli (ex. : on commence à réserver une place dans un train, et au moment de payer elle est plus dispo).
  Oracle ne propose pas de vrai serializable.
* `Read committed` (ex. : à partir du moment où je commence à réserver ma place dans le train, personne d’autre ne
  peut).
  Il peut y avoir quelques anomalies (lost updates, write skew).
  Toutes les BDD ne proposent pas le read committed.

Les autres modes n’ont pas été détaillés.

### Sauvegardes

* `Réplication` Pour les performances : indexes avec un stockage physique différent, vues matérialisées, cache, data
  warehouse...
* `Synchronous` On a toujours les dernières informations à jour, mais du coup il y a de la latence
* `Asynchronous` On peut avoir des données périmées

### Erreurs

S’il y a une erreur `COMMIT`, on ne sait pas dans quel état on est (ex. : on est à un DAB et on veut faire -100€ sur le
compte en banque du client.
Si on réessaie l’opération, on risque de lui enlever 200€.
Si on ne la refait pas, on risque de lui avoir donné 100€ gratuitement).

Une solution est de vérifier l’état de la transaction (PostgreSQL `txid_current()` / `txid_status()`, Oracle transaction
guard).

Sinon, pour limiter ce genre de problèmes, on peut :

* Déclarer des contraintes d’intégrité pour éviter les doublons
* Avoir des appels idempotents (`INSERT ... ON CONFLICT, MERGE`)

### Optimisation

Trois façons d’optimiser une requête :

* Faire en sorte qu’elle soit plus rapide
* La faire moins souvent
* Ne pas la faire du tout
