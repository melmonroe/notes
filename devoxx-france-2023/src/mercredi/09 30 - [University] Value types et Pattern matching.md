# [University] Value types et Pattern matching : 1 partout, données au centre

https://cfp.devoxx.fr/2023/talk/GNA-8922/Value_types_et_Pattern_matching_:_1_partout,_donnees_au_centre

Mercredi 12/04/23 9h30-12h30 - Salle Maillot

José PAUMARD (Oracle) [@JosePaumard](https://twitter.com/JosePaumard)> - Rémi FORAX (maître de conférences à
l’Université Gustave Eiffel)

## Abstract

> La modélisation de données en Java est l’un des aspects du langage qui a très peu évolué depuis 15 ans.
> Les choses changent, puisque Valhalla commence à publier des éléments intéressants.
> La version LW5 apporte des classes primitives et des value classes.
> Le projet Amber remet les données au centre des applications : les records permettent de meilleures modélisations, et
> le
> Pattern Matching ainsi que les Types Scellés permettent de mieux écrire les traitements.
> Dans le futur, Valhalla permettra d’unifier les types objet et les types primitifs et de gérer différemment les
> valeurs
> nulles.
> Amber va continuer de développer le pattern matching avec les déconstructeurs pour les classes classiques et les
> patterns nommés.
> Ces nouvelles approches vont apporter de nouvelles façons d’organiser les applications, de mieux distribuer le code en
> modules indépendants, de créer des données sous de nouvelles formes et d’avoir de meilleures performances pour nos
> traitements en mémoire.

La conférence était découpée en 2 parties :

* Une première partie d'1h30 dédiée au **projet Amber** (projet un peu fourre-tout, mais on parlera ici de pattern
  matching, de `sealed classes`, de `records` et de _data oriented programming_)
* Puis 1h30 dédiées aux **projets Valhalla + Lilliput** (`value types`, aka les _user-defined primitives_)

> Les fonctionnalités presentées lors de cette conférence sont encore à l’état de prototype. Elles ne seront intégrées à
> Java que d’ici plusieurs années.

Site recommandé : https://dev.java, géré par dévs d’Oracle

Podcast recommandé : https://inside.java

## Projet Amber

* En OOP, les API/interfaces sont plus importants que le code.
  Or, les API et les interfaces sont définies au début du projet et ne changent pas beaucoup par la suite.
  Au quotidien, un développeur travaille surtout avec les données.
* En DOP, les données sont plus importantes que le code.
  Les données changent souvent, et on aimerait que le compilateur nous aide quand ça arrive, ce qui n’est pas le cas en
  Java actuellement.

Par exemple, si on a :

```java
interface Named {};

public class City implements Named {...};
public class Department implements Named {...};
```

```java
public class BusinessCode {
  public String getName(Named named) {
    if (named instanceof City city) {
      return ...;
    }
  
    if (named instanceof Department department) {
      return ...;
    }
    
    throw new ...;
  }
}
```

Si on ajoute une nouvelle implémentation de `Named`, le compilateur ne le verra pas.

### Sealed classes

Pour ça, Amber introduit les `sealed classes` :

```java
sealed interface Named permits City, Department {};

public final sealed class City implements Named {...}; // une classe scellée doit être finale
public abstract non-sealed class Department implements Named {...}; // possiblité de créer des classes abstraites non scellées
```

On peut ensuite faire du pattern matching sur les types :

```java
public class BusinessCode {
  public String getName(Named named) {
    switch(named) {
        case null -> ...; // le case null est possible
        case City city -> ...;
        case Department department -> ...;
    }
  }
}
```

Le compilateur sait combien il y a d’implémentations et détecte si on a oublié une possibilité (à condition de ne pas
avoir mis de `default`).

WARNING: Pour que ça fonctionne, tout le code doit être dans le même module (pour qu’il soit compilé en même temps)

### Records

```java
public record City(String name) {};
```

Les `record` sont compilés en classes `final`, avec un `toString`, `equals` et `hashCode` (qu’on peut surcharger si on
n’aime pas l’implémentation).

On peut écrire un constructeur compact (_compact constructor_) si on veut valider les composants :

```java
public record City(String name) {
    
  public City {
    Objects.requireNonNull(name);
    // le this.name = ... est appelé automatiquement à la fin du constructeur compact,
    // pas la peine de le faire soi-même
  }
  
}

public record Department(String name, List<String> cities) { 
    
    public Department {
        Objects.requireNonNull(name);
        cities = cities.clone(); // copie défensive
        // c’est le paramètre qui est modifié, avant d’être affecté automatiquement en sortie
   }

    public City[] cities() {
        return cities.clone(); // double défense
    }

}
```

* "Composant" d’un `record` = la même chose qu’un champ pour les classes.
* Quand on désérialise un record, le constructeur compact est appelé (contrairement aux classes normales pour lesquelles
  aucun constructeur n’est appelé).
* On ne peut pas modifier le contenu d’un record avec la réflexion.
* Un record peut implémenter des interfaces, mais pas extends de classes.
* On peut mettre des champs statiques dedans ; mais pas de champs classiques.

**Record pattern** = pattern matching pour les records :

```java
public class BusinessCode {
  public String getName(Named named) {
      switch(named) {
      case City(String name, int population) -> name;
      ...
    }
  }
}
```

* Déconstruit le record (façon Javascript) ce qui permet d’accéder directement aux composants.
* Le compilateur détecte si on modifie les composants du record, ce qui nous oblige à modifier le pattern.

**Case when** :

```java
public class BusinessCode {
  public String getName(Named named) {
    switch(named) {
      case City(String name, int population) when name.equals("Paris") -> ...;
      case City(String name, int population)-> name;
      ...
    }
  }
}
```

![amber01.jpeg](../../img/amber01.jpg){width=500px}

-> maintenant si

### Dans le futur (pas encore implémenté)

Underscore pour les variables non utilisées :

```java
case City(String name, _) -> name;
```

Record patterns sur les classes :

```java
class Point {
    
  private int x, y;

  matcher (int x, int y) Point { // syntaxe provisoire
      matches this.x, this.y;
  }

}
```

Patterns nommés :

```java
Optional<String> opt = ...;

switch (opt) {
  case Optional.of(String s) -> ...;
  case Optional.empty() -> ...;
}
```

```java
final class Optional<T> {
    
    final private T value;
    final private boolean present;

    matcher(T t) of { // syntaxe provisoire
        if (present) match this.value;
        no-match;
    }

    matcher() empty { // syntaxe provisoire
        if (present) match;
        no-match;
    }

}
```

Déstructuration impérative :

```java
Point p = ...;
let Point(int x, int y) = p;
```

```java
for (let Map.Entry(var key, var value) : entrySet) {
    ...
}
```

NOTE: D’après les intervenants, les modifications apportées par Amber seront aussi, voire plus, importantes que les
lambda de Java 8.

## Projets Valhalla / Lilliput

But du projet : ne pas avoir à choisir entre un code lisible et les performances.

Les abstractions / la modélisation devraient avoir des performances équivalentes aux types primitifs :

* Types primitifs

```java
int[] populations = {...};

int totalPopulation = O;
for (int population :populations) {
    totalPopulation +=population;
}
```

* Objets

```java
record Population(int population) {};

Population populations = {...};

Population totalPopulation;
for (Population population :populations) {
    totalPopulation +=population.population;
}
```

### Abstraction for free (on stack)

Les objets temporaires devraient être gratuits :

* `Optional<T>`
* Vrais builders
* Chaînage : `Logger.warning().onConsole("Hello Devoxx!")`

Les valeurs wrappées devraient être gratuites (`Complex`, `Month`, `LocalDate`, etc).

### Improve information density (on heap)

Les `Integer` énormément de place par rapport aux `int`... Le header prend plus de place que l’information elle-même :

![valhalla01.jpeg](../../img/valhalla01.jpg){width=500px}

La façon dont sont gérés les objets posent 2 problèmes par rapport aux types primitifs :

* Des instructions supplémentaires (_operations on stack_)
* Des indirections supplémentaires (_layout in memory_)

![valhalla02.jpeg](../../img/valhalla02.jpg){width=500px}

### Values classes

Valhalla introduit les `value classes` (et les `value records`):

* Des classes qui abandonnent leur identité
* Les instances n’ont pas d’adresse mémoire

Mais continuent à :

* Hériter de `Object`
* Implémenter des interfaces
* Contenir des membres (methods, fields, nested classes…)

```java
public value class Population {
    final int population;
}
```

```java
Population population = new Population(23);
```

est équivalent à

```java
int population = 23;
```

* Tous les champs doivent être `final`.
* Problème : même comme ça, il y a toujours un endroit où on peut modifier la valeur d’un champ `final` : le
  constructeur.
  ** -> Le compilateur compile les constructeurs des `value class` en méthodes `static` :

* Code

```java
value class Population {

    final int population;

    Population(int population) {
        this.population = population;
    }

}
```

* Généré par `javac`

```java
value class Population {

    final int population;

    static Population<new>(int value){
        var tmp1 = aconst_init Population;
        var tmp2 = withfield Population.value(tmp1, value);
        return tmp2;
    }

}
```

L’objet est recréé à chaque modification, donc non modifiable.

* Certaines opérations nécessitent d’avoir un header, comme `==` qui compare les adresses.
  ** -> Quand on fait un  `==` sur une `value class`, il va faire un  `==` sur tous les champs (y compris les pointeurs,
  donc pas toujours fiable.)
  ** -> Ça rend `==` encore plus lent ; il faut donc encore moins l’utiliser qu’avant.
* `synchronized` sur une `value class` impossible, comme faire `new WeakRef(...)` : ça lance une exception.

* Benchmark avec des records

![valhalla03.jpeg](../../img/valhalla03.jpg){width=500px}

* Benchmark avec des value records

![valhalla04.jpeg](../../img/valhalla04.jpg){width=500px}

**Defining a non-null element**

On ne peut pas représenter `null`.

* On peut ajouter un `!` (emotion, _bang_) à toutes les utilisations (comme en Typescript)
* Fonctionne seulement si la classe est une `value class` avec une instance par défaut

```java
public value class Population {

    final int population;

    default Population(); // constructeur vide par défaut

    Population(int value) {
        this.value = value;
    }

}
```

```java
value record City(String name, Population! population) {}
```

```java
var populationArray = new Population![10];
List<Population!> populationList = new ArrayList();
```

Avec les `!` on atteint des performances similaires aux types primitifs :

![valhalla05.jpeg](../../img/valhalla05.jpg){width=500px}
![valhalla06.jpeg](../../img/valhalla06.jpg){width=500px}

### Concurence

Problèmes de concurrence si plusieurs lectures / écritures sur le même champ : on a pas de pointeur, donc on peut se
retrouver avec des incohérences.

* Solution rapide : ne pas mettre de `!`…
* Syntaxe proposée : rendre les `value classes` non-atomique avec un flag (pas implémenté pour l’instant)

### Conclusion

![valhalla07.jpeg](../../img/valhalla07.jpg){width=500px}

NOTE: Valhalla en est à son 5ᵉ prototype.
Le 6ᵉ devrait sortir dans les 2 ans à venir.
Les `value classes` arriveront dans un premier temps, les bangs et les constructeurs par défaut ensuite.

