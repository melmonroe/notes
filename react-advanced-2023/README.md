# React Advanced 2023

Le vendredi 20 octobre 2023 à The Brewery, Londres et le lundi 23 octobre 2023 en distanciel.

![intro01.jpg](img/intro01.jpg){width=300px} ![intro02.jpg](img/intro02.jpg){width=300px}

## TL;DR

- **React Forget**, nouveau compilateur en cours de développement chez Meta
- La quasi-intégralité des conférences ont évoqué les **server components** et **NextJS**
- L’intégralité des intervenants qui codaient en live l’ont fait avec **VSCode**
- Certains d’entre-eux utilisaient de l'**IA** (Copilot) pour les assister pendant leur présentation
- Sortie en mars 2024 de ***Fluent React*** chez O’Reilly (par Tejas Kumak)
- Les sujets abordés étaient très techniques, mais sur des temps beaucoup trop courts, rendant la plupart des présentations très difficiles à suivre
- Prochains évènements React en Europe :
    - React Day le 8 décembre 2023 à Berlin
    - React Summit le 14 juin 2024 à Amsterdam
    - React Advanced le 25 octobre 2024 à Londres

## Vendredi

(8:00 - Registration)

09:10 - [Piccard Track] Opening Ceremony

[09:20 - [Piccard Track] Simplifying Server Components](src/09%2020%20-%20%5BPiccard%20Track%5D%20Simplifying%20Server%20Compone%2014fd7a2987f08078aae1d5d13203bb50.md)

[10:00 - [Piccard Track] A Practical Guide for Migrating to Server Components](src/10%2000%20-%20%5BPiccard%20Track%5D%20A%20Practical%20Guide%20for%20Migr%2014fd7a2987f080caa01cc9d632dddaa4.md)

[10:40 - [Piccard Track] Leveraging the Event Loop for Blazing-Fast Applications!](src/10%2040%20-%20%5BPiccard%20Track%5D%20Leveraging%20the%20Event%20Loop%20%2014fd7a2987f08031ba81c44979c698a7.md)

(11:10 - Coffee Break)

[11:30 - [Piccard Track] The Unlikely Friendship Between React and Rust](src/11%2030%20-%20%5BPiccard%20Track%5D%20The%20Unlikely%20Friendship%20Be%2014fd7a2987f080f69434d81dbb156893.md)

[12:10 - [Piccard Track] Patterns for Performance](src/12%2010%20-%20%5BPiccard%20Track%5D%20Patterns%20for%20Performance%2014fd7a2987f080048d10c7ac9201d0a9.md)

(12:40 - Lunch)

[13:40 - [Piccard Track] The State of The State In The App Router](src/13%2040%20-%20%5BPiccard%20Track%5D%20The%20State%20of%20The%20State%20In%20%2014fd7a2987f08048918fe27585080440.md)

[~~14:20 - [Piccard Track] Visualize State Management With XState~~](src/14%2020%20-%20[Piccard%20Track]%20Visualize%20State%20Management%20With%20XState.md)

[14:20 - [Van Dover Track] How Popovers Are About to Become a Whole Lot Easier to Build](src/14%2020%20-%20%5BVan%20Dover%20Track%5D%20How%20Popovers%20Are%20About%20t%2014fd7a2987f080a18f58ef9d883bc013.md)

[15:00 - [Piccard Track] Understanding Idiomatic React](src/15%2000%20-%20%5BPiccard%20Track%5D%20Understanding%20Idiomatic%20Re%2014fd7a2987f080d59f37e452ed31000b.md)

(15:30 - Coffee Break)

[16:00 - [Piccard Track] Hydration, Islands, Streaming, Resumability… Oh My!](src/16%2000%20-%20%5BPiccard%20Track%5D%20Hydration,%20Islands,%20Stream%2014fd7a2987f080ce86edf43e51a8473a.md)

[16:40 - [Piccard Track] Why Everybody Needs a Framework](src/16%2040%20-%20%5BPiccard%20Track%5D%20Why%20Everybody%20Needs%20a%20Fram%2014fd7a2987f080699babf6a692a6052f.md)

17:10 - [Piccard Track] Closing Ceremony

(17:20 - Break before the party)

(19:00 - Afterparty)