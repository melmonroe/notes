# 16:40 - [Piccard Track] Why Everybody Needs a Framework

Tejas KUMAR (Shopify, Australie) <[GitHub](https://github.com/TejasQ/) /  [@tejaskumar_](https://twitter.com/tejaskumar_)>

## Abstract

> The prevalent advice on how to use React today is to use a framework: either to start with one or to incrementally adopt one.
This deep-dive talk will explore why, and how to go about this with live code examples.

## Notes

- React n’est pas déjà un framework ?
    - On laisse ce débat aux réseaux sociaux
    - On parle ici de NextJS / Remix
- Vote à main levée : la plupart des gens utilisent un framework, et parmi ceux-ci la plupart font du server-side rendering
- Si vous utilisez pas de framework, vous allez finir par en écrire un vous-même
- Que fonts les frameworks ?
    - Server rendering
    - Isomorphic routing
    - Data fetching : "server components is the answer to data fetching, period"
- Le speaker sort ***Fluent React*** chez O’Reilly en mars 2024

> "If you use React, you should be using a framework.
If your existing app doesn’t use a framework, you should incrementally migrate to one.
If you’re creating a new React project, you should use a framework from the beginning."
> 

— Andrew CLARK
