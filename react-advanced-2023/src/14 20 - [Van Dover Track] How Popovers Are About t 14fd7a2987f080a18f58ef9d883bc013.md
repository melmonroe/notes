# 14:20 - [Van Dover Track] How Popovers Are About to Become a Whole Lot Easier to Build

Hidde DE VRIES (gouvernement néérlandais) <[GitHub](https://github.com/hidde/) /  [@hdv](https://twitter.com/hdv)>

## Abstract

> You’ve probably build one of these before: tooltips, date pickers, menus, teaching UI… they’re all examples of “popover” content.
Good news: it’s going to get a whole lot easier to build these, with some proposed web platform features.
You might not even need JavaScript.
In this talk, you’ll learn all about the upcoming ‘popover’ attribute, when to use modality and access to the top layer.

## Notes

- On avait déjà la balise HTML `<dialog>`, bien supportée par les navigateurs modernes
- Un nouvel attribut est en train de faire son apparition : `popover`


```html
<div popoverid="p"/>
<button popovertarget="p"/>
```

- modale = mesure drastique qd on vueut une réponse tout de suite. si le reste du sute est encore accessible c un simple popover.
    - `element.show()` : simple popover, le reste du site reste accessible
    - `element.showModal()` : modale - mesure drastique quand on veut réponse tout de suite
- Plein de paramètres sont disponibles (`lightdismiss`…)
- Les popover s’affichent sur le "top layer", qui est au dessus de tout (même le `z-index 999`)
