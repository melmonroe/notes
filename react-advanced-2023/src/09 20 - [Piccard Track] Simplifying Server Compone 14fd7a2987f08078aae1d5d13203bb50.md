# 09:20 - [Piccard Track] Simplifying Server Components

Mark DALGLEISH (Shopify, Australie) <[GitHub](https://github.com/markdalgleish/) /  [@markdalgleish](https://twitter.com/markdalgleish)>

## Abstract

> Server Components are arguably the biggest change to React since its initial release but many of us in the community have struggled to get a handle on them.
In this talk we’ll try to break down the different moving parts so that you have a good understanding of what’s going on under the hood, and explore the line between React and the frameworks that are built upon it.

## Notes

- Essayons de simplifier quelques termes :
    - Qu’est-ce qu’un composant ?
    Une *pure function*
    - Qu’est-ce que JSX ?
    Des appels de fonctions
    - Qu’est-ce que les *server components* ?
    Des composants qui ne tournent que sur le serveur
        - Réponse pas satisfaisante
- Si on fait des server components, on veut probablement utiliser **NextJS**
    - Attention : les *server components* sont quelque chose de très bas niveau, les différents frameworks (NextJS, Remix...) sont obligés de faire des choix, parfois différents
- *Server components* VS *server side rendering*
    - *Server side rendering* : server side pre-rendering
    - *Server components* : serialized virtual DOM over the network
- Comment on fait pour interagir avec un *server component* ?
    - On envoit un "client manifest" qui contient des ids qui permettent de faire le mapping
- **"Server components are juste virtual DOM over the network"**