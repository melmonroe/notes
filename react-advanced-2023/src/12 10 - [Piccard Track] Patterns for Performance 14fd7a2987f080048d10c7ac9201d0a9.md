# 12:10 - [Piccard Track] Patterns for Performance

Stephen COOPER (AG Grid, Royaume-Uni) <[GitHub](https://github.com/StephenCooper/) /  [@scooperdev](https://twitter.com/scooperdev)>

## Abstract

> When working with React it is important that we use the right tool for the right job.
If not we will be missing out on the best performance and developer experience.
While optimising AG Grid React Table we discovered a number of patterns that made significant improvements to the tables performance.

## Notes

- AG Grid présente une étude de cas qu’ils ont rencontré : quand on redimensionne une cellule dont le *render* est customisé, ça laggue
- Première solution évidente mais pas suffisante : `useMemo`
- Quand on change la largeur, la cellule se re-render alors qu’a priori la structure ne va pas changer, c’est seulement le style qui va changer
    - Ils ont essayé de modifier directement le style au lieu de re-render
    - Ça cause un autre bug : `useEffect` n’est appelé qu’après le premier render, donc on affiche d’abord une lergeur à 0, puis ça "saute" à la bonne taille
- `useLayoutEffect` : comme `useEffect` mais se lance avant que le navigateur rafraîchisse l’écran
    - Bloque l’affichage, peu affecter les performances
- La solution : `useRef callback` → synchrone → 2 renders au lieu de 254

![patterns01.jpg](../img/patterns01.jpg){width=500px}