# 13:40 - [Piccard Track] The State of The State In The App Router

Jack HERRINGTON (Blue Collar Coder, USA) <[GitHub](https://github.com/jherr/) /  [@jherr](https://twitter.com/jherr)>

## Abstract

> NextJS 13.4 changed the game with the App Router and React Server components.
The change to the state management model looks simple to start, but gets complex very quickly as you try to create the practical every-day user experiences that were straightforward in the pages model.
We’ll talk about the different state management models, how they work in the App Router, and how to decide which you should use.

## Notes

- Rule#0: You might not need a state manager (React/**NextJS** en embarquent déjà un)
- Zustand Store : pratique pour avoir différents stores dans différentes branches de l’application
- Rule#2: RSC don’t store data
- Rule#3: Separate mutable & immutable data
- Contextes React : peuvent s’utiliser pour stocker des données qui ne changent pas trop souvent (infos utilisateur…)
