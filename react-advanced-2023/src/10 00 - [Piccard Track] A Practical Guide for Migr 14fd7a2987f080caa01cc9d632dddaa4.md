# 10:00 - [Piccard Track] A Practical Guide for Migrating to Server Components

Fredrik HÖGLUND (Ephem AB, Suède) <[GitHub](https://github.com/Ephem/) /  [@ephemjs](https://twitter.com/ephemjs)>

## Abstract

> Server Components are the hot new thing, but so far much of the discourse around them has been abstract.
Let’s change that.
This talk will focus on the practical side of things, providing a roadmap to navigate the migration journey.
Starting from an app using the older Next.js pages router and React Query, we’ll break this journey down into a set of actionable, incremental steps, stopping only when we have something shippable that’s clearly superior to what we began with.
We’ll also discuss next steps and strategies for gradually embracing more aspects of this transformative paradigm.

## Notes

- Annonce : TanStack Query v5 vient de sortir
- Objectifs
    - Migration incrémentale
    - Apprendre pas à pas
    - Inclure toute l’équipe
    - Garder le plus possible de code existant
- Dans cette conférence, on utilisera **NextJS** et React Query
- Étape 1 : "plan & prepare"
    - Faire l’inventaire du code
    - Faire l’inventaire des librairies utilisées
    - Penser au déploiement, au devops et aux tests
    - Penser à l’authentification
    - Réfléchir à l’ordre dans laquelle effectuer la migration : choisir d’abord une page simple, ensuite une page plus compliquées ; déterminer quelles pages doivent être migrées ensemble
- Étape 2 : nettoyer le code existant
    - Tout passer en `StrictMode`
    - Mettre à jour les librairies utilisées
    - Etc.
- Étapes 3 et 4
    - Migrer une page, "add layout" (?)
    - Recommencer avec une autre page
- Étape 5 : "move stuff to the server"

## Q&A

- Pourquoi utiliser des *server components* ?
    - Ca dépend de l’application ; on a plus ou moins à y gagner.
    - Les *serveur components* peuvent permettre de rendre l’application plus fluide pour l’utilisateur
    - Le fait que le cache soit géré côté serveur permet de le centraliser pour tous les utilisateurs
    - Inconvénient : ça coûte plus cher en serveurs