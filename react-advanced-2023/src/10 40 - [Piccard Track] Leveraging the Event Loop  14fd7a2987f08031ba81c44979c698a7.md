# 10:40 - [Piccard Track] Leveraging the Event Loop for Blazing-Fast Applications!

Michael DI PRISCO (Jointly, Italie) <[GitHub](https://github.com/Cadienvan/) /  [@Cadienvan](https://twitter.com/Cadienvan)>

## Abstract

> Leveraging the Event Loop for Blazing-Fast Applications!
Some weeks ago I made my own implementation of signals, capable of updating 300k times the DOM in circa 600ms.
Leveraging the Microtask Queue, I was able to bring that number down to 6ms, effectively improving performances by 100x.
I wanna share how the Microtask Queue works and how you can leverage the Event Loop to improve performances.

## Notes

- Javascript est monothreadé, l’event loop permet de simuler un pseudo multi-threading
- Main concepts of the event loop
    - Run-to-completion operations
    - Leveraging queues to free up the thread
    - The timeout isn’t granted
- L’event loop prend des tâches dans la *macrotask queue* et la *microtask queue* pour les placer dans la *call stack* et simuler un multi-threading
    - ***Microtask queue*** (promesses…)
    - ***Macrotask queue*** (`setTimeout`, `setInterval`, évènements clavier/souris…)
- Il est possible de placer manuellement des tâches dans la *microtask queue*
- Une autre version de la conférence est disponible ici : [https://www.youtube.com/watch?v=xcJuwHcspyY](https://www.youtube.com/watch?v=xcJuwHcspyY)

![eventloop01.jpg](../img/eventloop01.jpg){width=500px}