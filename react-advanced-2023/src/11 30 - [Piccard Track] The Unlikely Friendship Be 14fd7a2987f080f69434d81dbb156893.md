# 11:30 - [Piccard Track] The Unlikely Friendship Between React and Rust

Sara VIEIRA (axo.dev, Allemagne) <[GitHub](https://github.com/SaraVieira/) /  [@NikkitaFTW](https://twitter.com/NikkitaFTW)>

## Notes

- **Tauri** : permet d’appeler du Rust depuis React
- Electron vs Tauri : Rust est plus rapide, mais aussi plus compliqué
