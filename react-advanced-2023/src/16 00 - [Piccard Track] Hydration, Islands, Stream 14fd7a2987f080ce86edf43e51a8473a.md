# 16:00 - [Piccard Track] Hydration, Islands, Streaming, Resumability… Oh My!

Matheus ALBUQUERQUE (Medallia, République Tchèque) <[GitHub](https://github.com/ythecombinator/) /  [@ythecombinator](https://twitter.com/ythecombinator)>

## Notes

102 slides en 20 minutes, difficile à suivre.

Disponible ici :
[https://speakerdeck.com/ythecombinator/react-advanced-london-2023](https://speakerdeck.com/ythecombinator/react-advanced-london-2023)
