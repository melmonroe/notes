# 15:00 - [Piccard Track] Understanding Idiomatic React

Joe SAVONA (Meta, USA) <[GitHub](https://github.com/josephsavona/) /  [@en_JS](https://twitter.com/en_JS)> - Mofei ZHANG (Meta, USA) <[GitHub](https://github.com/mofeiZ/)>

## Abstract

> React provides a contract to developers-uphold certain rules, and React can efficiently and correctly update the UI.
In this talk we’ll explore these rules in depth, understanding the reasoning behind them and how they unlock new directions such as automatic memoization.

## Notes

Talk sur **React Forget**, un nouveau compilateur React en cours de développement chez Meta.

- Constat de départ : si on regarde l’exemple de code suivant, si seuls les `headers` changent, alors on va quand même recalculer `filteredVideos` et de ce fait re-render `VideoList` alors qu’elle n’aura pas changé

![react-forget01.png](../img/react-forget01.png){width=500px}
- Solution simple et évidente : utiliser `useMemo`

![react-forget02.png](../img/react-forget02.png){width=500px}
- En réalité, la grande majorité des problèmes de performances rencontrés avec React peuvent se résoudre de cette façon
- Inconvénient : le code est moins clair
    - On ne dit pas seulement ce que l’UI doit faire, mais comment elle doit le faire
    - Ça va à l’encontre de la vision de meta pour React
- React Forget est capable de faire ça tout seul : il mémoïse automatiquement les composants et les hooks
- Comment ça fonctionne ?
    - React Forget est un compilateur "bas niveau" dans le sens où il est codé *pour* React et connaît parfaitement ses règles
    - "C’est pas de la magie noire, ça s’appelle de la science"
- Le développement est quasiment terminé, et ils sont actuellement en phase de tests avec plusieurs partenaires

![react-forget03.png](../img/react-forget03.png){width=500px}

## Q&A

- A terme, l’objectif est complètement supprimer `useMemo` et `useCallback` de React
- Pourquoi ça s’appelle React Forget et pas React Remember ?
    - Ils ont pour règle de donner des noms qui commencent par F
    - "We are New-Yorkers, we like f-words"
- Quand est-ce que ça sort ?
    - "Il faut oublier React Forget jusqu’à son lancement"